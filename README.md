# VueJS VKUI

## Installation
```
npm install @vue-vkui/vkui
```

### Usage
```
import Vue from 'vue'
...

import '@vue-vkui/vkui'

import '@vue-vkui/vkui-icons'
import '@vue-vkui/vkui-icons/dist/vkui-icons.css'

import connect from '@vkontakte/vkui-connect'

import PortalVue from 'portal-vue'
Vue.use(PortalVue)
...

```
